import java.util.Scanner;

public class Library {
    private final DataStorage dataStorage = new DataStorage();
    private final Scanner scanner = new Scanner(System.in);

    public void start() {
        System.out.println("Welcome to the Library!");
        printMenu();
        while (true) {
            String userInput = scanner.nextLine().toLowerCase();
            executeAction(userInput);
        }
    }

    private void printMenu() {
        String menuOptions = """
                Please, select one of the following actions by typing the option's number and pressing enter key:
                [1] show all BOOKS int the library
                [2] show all READERS registered in the library
                type “EXIT” to stop the program and EXIT!
                """;
        System.out.println(menuOptions);
    }

    private void executeAction(String input) {
            switch (input) {
                case "1" ->
                    printAllBooks();
                case "2" ->
                    printAllReaders();
                case "exit" -> {
                    System.out.println("Goodbye!");
                    System.exit(0);
                }
                default ->
                    System.out.println("Incorrect input. Please, try again:");
            }
            System.out.println();
            printMenu();
        }

    private void printAllBooks() {
        dataStorage.getBooks().forEach(System.out::println);
    }

    private void printAllReaders() {
        dataStorage.getReaders().forEach(System.out::println);
    }

}
