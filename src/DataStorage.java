import java.util.ArrayList;
import java.util.List;

public class DataStorage {
    private final List<Book> books = new ArrayList<>();
    private final List<Reader> readers = new ArrayList<>();

    public DataStorage() {
        books.add(new Book(0, "Shigumo", "Boris Akunin"));
        books.add(new Book(1, "One Hundred Years of Solitude", "Gabriel Garcia Marquez"));
        books.add(new Book(2, "Pride and Prejudice", "Jane Austen"));

        readers.add(new Reader(0, "Andrew Row"));
        readers.add(new Reader(1, "Elis Larson"));
        readers.add(new Reader(2, "Petr Manulov"));
    }


    public List<Book> getBooks() {
        return books;
    }

    public List<Reader> getReaders() {
        return readers;
    }
}
